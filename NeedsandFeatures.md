﻿Shopping List for Android
Needs


* Needs persistent data. Shopping list items need to be saved for later.
* Items need to be checked off
* Items need to be able to be reordered on the list (optional)
* Phone needs a camera.
* Android version to be determined.
* List needs to be able to grow as items are added. An unknown amount of items may be added to list.
* List needs to be responsive. A 100+ item list (someone does once a month shopping?) needs to be able to scroll without causing lag.


Features


* Camera to capture shopping list from whiteboard.
* Items can be checked off as the item is bought.
* Algorithm to determine routinely bought items and prompt user if it is near time to buy again (future feature).
* No information leaves the user’s phone (at this time).


Requirements


* An Android phone using a later to be determined version.
* A phone with a camera for camera capture of shopping list.