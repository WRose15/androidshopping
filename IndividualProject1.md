#﻿Vision Statement 0.76


For any Android user who wants to use their phone as a shopping list. This mobile shopping list is a mobile app that will allow the user to enter their items on a list stored on their phone. It will also add items captured from a picture of a list written on a whiteboard. The user can also check off each item as they pick it up for deletion later. The app will have an alarm that the user can set as a reminder and a learning algorithm can prompt the user for frequently bought items if they’re not on the list.