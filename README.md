# My project

This is my individual senior project for Western Oregon University. 

This android app will use Google's OCR API in order to capture a shopping list written on a whiteboard and to convert it into editable text. The user of the app would be able to add shopping items to the list by either typing it or using their camera.