package com.rosecorp.shoppinglist;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Wayne on 4/18/2017.
 * The interface for the database. Both main activity listnames and item lists
 * are handled here.
 */

 class GroceryDataSource {

    private  SQLiteDatabase db;
    private  DatabaseHandler dbHelper;

    private String[] allColumns = {DatabaseHandler.COLUMN_ID, DatabaseHandler.COLUMN_ITEM,
            DatabaseHandler.COLUMN_LIST_ID, DatabaseHandler.COLUMN_ISSELECTED};
    private String[] allListColumns = {DatabaseHandler.COLUMN_ID2, DatabaseHandler.COLUMN_LIST_NAME};
    // SQLite takes ints and strings only. This is for conversion.
    private final int SELECTED_IS_FALSE = 0;
    private final int SELECTED_IS_TRUE = 1;

     GroceryDataSource(Context context)
    {
        dbHelper = DatabaseHandler.getsInstance(context);
    }

     void open() throws SQLException
    {
        try {
            db = dbHelper.getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

     void close()
    {
        dbHelper.close();
    }

    /**
     *  Creates item in database
     * @param itemName String value of item to be added.
     * @return returns itemName once it is added to the database.
     */
     ItemModel createItem(String itemName, long listID)
    {
        ContentValues values = new ContentValues();

        values.put(DatabaseHandler.COLUMN_ITEM, itemName);
        values.put(DatabaseHandler.COLUMN_LIST_ID, listID);
        values.put(DatabaseHandler.COLUMN_ISSELECTED, SELECTED_IS_FALSE);
        long insertID = db.insert(DatabaseHandler.TABLE_GROCERY_LIST, null, values);
        Cursor cursor = db.query(DatabaseHandler.TABLE_GROCERY_LIST, allColumns,
                DatabaseHandler.COLUMN_ID + " = " + insertID, null, null, null, null);
        cursor.moveToFirst();
        ItemModel newItem = cursorToItem(cursor);
        cursor.close();
        return newItem;
    }

    /**
     * Deletes item from database. Does not leave record of item.
     * @param item ItemModel to delete.
     */
     void deleteItem(ItemModel item)
    {
        long id = item.getId();
        db.delete(DatabaseHandler.TABLE_GROCERY_LIST, DatabaseHandler.COLUMN_ID
                    + " = " + id, null);
    }

    /**
     * Modifies an existing list item in the database.
     * @param item Item to modify with the modification already set.
     */
     void modifyItem(ItemModel item)
    {
        long id = item.getId();
        ContentValues values = new ContentValues();
        int isSelected;
        if(item.isSelected())
        {
            isSelected = SELECTED_IS_TRUE;
        } else {
            isSelected = SELECTED_IS_FALSE;
        }
        values.put(DatabaseHandler.COLUMN_ID, id);
        values.put(DatabaseHandler.COLUMN_LIST_ID, item.getListID());
        // set isSelected to false. Would need to convert boolean to int otherwise.
        values.put(DatabaseHandler.COLUMN_ISSELECTED, isSelected);
        values.put(DatabaseHandler.COLUMN_ITEM, item.getItemName());
        db.update(DatabaseHandler.TABLE_GROCERY_LIST, values, DatabaseHandler.COLUMN_ID + " = " + id, null);
    }

    /**
     * Get all of the items that are associated with a single list.
     * @param listID long ID of the list
     * @return List of itemModels
     */
     List<ItemModel> getAllItems(long listID)
    {
        List<ItemModel> groceryList = new ArrayList<>();
        Cursor cursor = db.query(DatabaseHandler.TABLE_GROCERY_LIST, allColumns, DatabaseHandler.COLUMN_LIST_ID + " = " + listID, null, null,
                null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            ItemModel item = cursorToItem(cursor);
            groceryList.add(item);
            cursor.moveToNext();
        }
        cursor.close();
        return groceryList;
    }

    /**
     * Returns a long with the number of items in the selected list
     * @param listID long value of the list ID
     * @return count of items in list
     */
    long getItemCount(long listID)
    {
        return DatabaseUtils.queryNumEntries(db, DatabaseHandler.TABLE_GROCERY_LIST,
                DatabaseHandler.COLUMN_LIST_ID + " = " + listID);
    }

    long getSelectedItemcount(long listID)
    {
        return DatabaseUtils.queryNumEntries(db, DatabaseHandler.TABLE_GROCERY_LIST,
                DatabaseHandler.COLUMN_LIST_ID + " = " + listID + " AND "
                + DatabaseHandler.COLUMN_ISSELECTED + " = " + SELECTED_IS_TRUE);
    }

    /**
     * Converts the database data into an ItemModel object.
     * @param cursor a cursor
     * @return ItemModel
     */
    private ItemModel cursorToItem(Cursor cursor)
    {
        // Magic numbers are bad. MMMmmmkay?
        // Get the column index with the item name.
        int nameCol = cursor.getColumnIndex(DatabaseHandler.COLUMN_ITEM);
        int idCol = cursor.getColumnIndex(DatabaseHandler.COLUMN_ID);
        int selectCol = cursor.getColumnIndex(DatabaseHandler.COLUMN_ISSELECTED);
        int listID = cursor.getColumnIndex(DatabaseHandler.COLUMN_LIST_ID);

        // add the row info to the return object
        ItemModel item = new ItemModel(cursor.getString(nameCol));
        item.setId(cursor.getLong(idCol));
        item.setListID(cursor.getLong(listID));
        // transforms int to boolean since sqlite does not handle booleans
        boolean check = (cursor.getInt(selectCol) == 1)? true : false;
        item.setSelected(check);

        return item;
    }

    /**
     * Adds a list to the database
     * @param name Name of list to add
     * @return ListModel object
     */
     ListModel createList(String name) {

        ContentValues values = new ContentValues();
        values.put(DatabaseHandler.COLUMN_LIST_NAME, name);

        // insert name to create a new row and return the long id
        long insertID = db.insert(DatabaseHandler.TABLE_LIST_LIST, null, values);

        // query the db to get all the info in the row
        Cursor cursor = db.query(DatabaseHandler.TABLE_LIST_LIST, allListColumns,
                DatabaseHandler.COLUMN_ID2 + " = " + insertID, null, null, null, null);
        cursor.moveToFirst();
        ListModel newList = cursorToList(cursor);
        cursor.close();
        return newList;
    }

    /**
     * Converts the database row to a ListModel object
     * @param cursor a cursor
     * @return ListModel
     */
    private ListModel cursorToList(Cursor cursor)
    {
        // get column indexes without magic numbers.
        int listID = cursor.getColumnIndex(DatabaseHandler.COLUMN_ID2);
        int name = cursor.getColumnIndex(DatabaseHandler.COLUMN_LIST_NAME);

        ListModel list = new ListModel();
        list.setListID(cursor.getLong(listID));
        list.setListName(cursor.getString(name));

        return list;
    }

    /**
     * Deletes the list and all of the items in the list.
     * @param list list to delete.
     */
     void deleteSingleList(ListModel list)
    {
        long id = list.getListID();
        db.delete(DatabaseHandler.TABLE_LIST_LIST, DatabaseHandler.COLUMN_ID2
                + " = " + id, null);
        db.delete(DatabaseHandler.TABLE_GROCERY_LIST, DatabaseHandler.COLUMN_LIST_ID
                + " = " + id, null);
    }

    /**
     * Updates the list name from the main activity
     * @param list Name of list to update.
     */
     void modifyListName(ListModel list)
    {
        long id = list.getListID();
        ContentValues values = new ContentValues();
        values.put(DatabaseHandler.COLUMN_ID2, id);
        values.put(DatabaseHandler.COLUMN_LIST_NAME, list.getListName());
        db.update(DatabaseHandler.TABLE_LIST_LIST, values, DatabaseHandler.COLUMN_ID2
                + " = " + id, null);
    }

    /**
     * Retrieves all of the list names in the database
     * @return List info
     */
     List<ListModel> getAllLists()
    {
        List<ListModel> listOfLists = new ArrayList<>();
        Cursor cursor = db.query(DatabaseHandler.TABLE_LIST_LIST, allListColumns, null, null, null,
                null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            ListModel list = cursorToList(cursor);
            listOfLists.add(list);
            cursor.moveToNext();
        }
        return listOfLists;
    }

}
