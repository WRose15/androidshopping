package com.rosecorp.shoppinglist;

/**
 * Created by Wayne on 5/28/2017.
 */

class ListModel {

    private long listID;
    private String listName;

    public long getListID() { return this.listID;}
    public void setListID(long id) { this.listID = id;}
    public String getListName() { return this.listName; }
    public void setListName(String name) { this.listName = name; }

}
