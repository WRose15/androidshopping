package com.rosecorp.shoppinglist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.view.ContextMenu;


import java.util.List;

/**
 * Created by Wayne on 5/28/2017.
 * List view adapter for the main activity
 * Displays the names of the lists and allows to select an individual one.
 */

class MainListViewAdapter extends ArrayAdapter<ListModel>
{
    private final List<ListModel> listOfLists;
    private final Activity context;
    GroceryDataSource ds;

    MainListViewAdapter(Activity context, List<ListModel> listOfLists, GroceryDataSource ds)
    {
        super(context, R.layout.mainlists, listOfLists);
        this.context = context;
        this.listOfLists = listOfLists;
        this.ds = ds;
    }

    private static class ViewHolder
    {
        TextView text;
        Button button;
    }

    /**
     * Builds the individual lines in the view. This is where the onClickListeners reside in
     * order to get the proper positioning.
     * @param position int position in the list
     * @param convertView View
     * @param parent Parent
     * @return views
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        View view = null;
        if (convertView == null)
        {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.mainlists, parent, false);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) view.findViewById(R.id.listlabel);
/*            viewHolder.text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ListGroupActivity) context).viewList(v);
                }
            });*/
            viewHolder.button = (Button) view.findViewById(R.id.listbutton);
            viewHolder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ListGroupActivity) context).viewList(v);
                }
            });
            view.setTag(viewHolder);
            viewHolder.text.setTag(listOfLists.get(position));
            viewHolder.button.setTag(listOfLists.get(position));
        } else {
            view = convertView;
            ((ViewHolder)view.getTag()).text.setTag(listOfLists.get(position));
            ((ViewHolder)view.getTag()).button.setTag(listOfLists.get(position));
        }
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.text.setText(listOfLists.get(position).getListName());
        holder.button.setText(Long.toString(ds.getItemCount(listOfLists.get(position).getListID())));

        return view;
    }

    /**
     * Modify list object in database.
     * @param list list
     */
    void modifyList(ListModel list)
    {
        if(list != null)
        {
            ds.modifyListName(list);
        }
    }

    void deleteList(ListModel list)
    {
        listOfLists.remove(list);
        ds.deleteSingleList(list);
    }


}
