package com.rosecorp.shoppinglist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.view.ContextMenu;


import java.util.Iterator;
import java.util.List;

/**
 * Created by Wayne on 4/15/2017.
 */

public class ListViewAdapter extends ArrayAdapter<ItemModel>
{
    private final List<ItemModel> groceryList;
    private final Activity context;
    GroceryDataSource ds;

    public ListViewAdapter(Activity context, List<ItemModel> groceryList, GroceryDataSource ds)
    {
        super(context, R.layout.rowbuttonlayout, groceryList);
        this.context = context;
        this.groceryList = groceryList;
        this.ds = ds;
    }

    static class ViewHolder
    {
        protected TextView text;
        protected CheckBox checkBox;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View view = null;
        if (convertView == null)
        {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.rowbuttonlayout, parent, false);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView)view.findViewById(R.id.label);
            viewHolder.checkBox = (CheckBox)view.findViewById(R.id.check);
            viewHolder.checkBox
                    .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
                    {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                        {
                            ItemModel element = (ItemModel) viewHolder.checkBox.getTag();
                            element.setSelected(buttonView.isChecked());
                            ds.modifyItem(element);
                        }
                    });
            view.setTag(viewHolder);
            viewHolder.checkBox.setTag(groceryList.get(position));
        } else {
            view = convertView;
            ((ViewHolder)view.getTag()).checkBox.setTag(groceryList.get(position));
        }
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.text.setText(groceryList.get(position).getItemName());
        holder.checkBox.setChecked(groceryList.get(position).isSelected());
        return view;
    }

    void modifyItem(ItemModel item)
    {

        if(item != null)
        {
            ds.modifyItem(item);
        }
    }
    /**
     * Deletes the items that have been checked. Steps through each ItemModel
     * in the list and deletes it if it has been selected. Deletes both from
     * the list used for the listView and from the database. Removes from the
     * database first.
     */
    void deleteChecked()
    {
        for (Iterator<ItemModel> it = groceryList.iterator(); it.hasNext();)
        {
            ItemModel item = it.next();
            if(item.isSelected())
            {
                ds.deleteItem(item);
                it.remove();
            }
        }
    }

    void deleteItem(ItemModel item)
    {
        groceryList.remove(item);
        ds.deleteItem(item);
    }
}
