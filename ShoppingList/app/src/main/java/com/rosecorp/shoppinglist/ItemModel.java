package com.rosecorp.shoppinglist;

/**
 * Created by Wayne on 4/15/2017.
 */

public class ItemModel
{
    private long id;
    private String itemName;
    private boolean selected;
    private long listID;

    public ItemModel(String item)
    {
        this.itemName = item;
        selected = false;
    }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public String getItemName()
    {
        return itemName;
    }

    public void setItemName(String name)
    {
        this.itemName = name;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

    public long getListID() { return listID; };

    public void setListID(long id ) { this.listID = id; };
}
