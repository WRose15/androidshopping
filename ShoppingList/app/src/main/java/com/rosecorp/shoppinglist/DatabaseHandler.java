package com.rosecorp.shoppinglist;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Wayne on 4/18/2017.
 * Handles creation of the database.
 */

class DatabaseHandler extends SQLiteOpenHelper {

    //Singleton Instance
    private static DatabaseHandler sInstance;


    // Database Constants
    private static final int DATABASE_VERSION = 5;
    private static final String DATABASE_NAME = "shoppingList.db";
    // Database Table names
    static final String TABLE_GROCERY_LIST = "groceryList";
    static final String COLUMN_ID = "_id";
    static final String COLUMN_ITEM = "item";
    static final String COLUMN_LIST_ID = "listID";
    static final String COLUMN_ISSELECTED = "selected";

    static final String TABLE_LIST_LIST = "itemLists";
    static final String COLUMN_ID2 = "_id";
    static final String COLUMN_LIST_NAME = "listName";


    // Database creation SQL statement
    private static final String DATABASE_CREATE_GROCERYLIST = "create table "
            + TABLE_GROCERY_LIST
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_ITEM + " text not null, "
            + COLUMN_LIST_ID + " integer not null, "
            + COLUMN_ISSELECTED + " int not null "
            + ");";

    private static final String DATABASE_CREATE_LIST_LIST = "create table "
            + TABLE_LIST_LIST
            + "("
            + COLUMN_ID2 + " integer primary key autoincrement, "
            + COLUMN_LIST_NAME + " text not null "
            + ");";

    // Create a Singleton instance
     static synchronized DatabaseHandler getsInstance(Context context)
    {
        if (sInstance == null)
        {
            sInstance = new DatabaseHandler(context.getApplicationContext());
        }
        return sInstance;
    }

    /**
     * Constructor is private to prevent a direct instantiation,
     * Make a call to the method getInstance() instead.
     * @param context It is what it is
     */
    private DatabaseHandler(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DATABASE_CREATE_GROCERYLIST);
        db.execSQL(DATABASE_CREATE_LIST_LIST);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        Log.w(DATABASE_NAME, "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data.");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROCERY_LIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LIST_LIST);
        onCreate(db);
    }
}

