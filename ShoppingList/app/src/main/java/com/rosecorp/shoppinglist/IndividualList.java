package com.rosecorp.shoppinglist;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;

import java.util.List;

public class IndividualList extends ListActivity {

    List<ItemModel> myList;
    ListViewAdapter adapter;
    ListView listview;
    private GroceryDataSource ds;
    ItemModel editItem;
    long listID;

    private boolean autoFocus;
    private boolean useFlash;
    private boolean addNew;

    private static final int RC_OCR_CAPTURE = 9003;
    private static final String TAG = "IndividualList";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        listID = intent.getLongExtra(ListGroupActivity.LIST_ID, 1);
        setTitle(intent.getStringExtra(ListGroupActivity.LIST_NAME));

        ds = new GroceryDataSource(this);
        ds.open();
        myList = ds.getAllItems(listID);

        autoFocus = true;
        useFlash = false;
        addNew = true;

        adapter = new ListViewAdapter(this, myList, ds);
        listview = getListView();
        setListAdapter(adapter);

        listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        registerForContextMenu(listview);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ds.open();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ds.close();
    }

    /**
     * Connect to the add button to add an item to the grocery list.
     *
     * @param v view that the button is in.
     */
    public void addItems(View v) {
        EditText editText = (EditText) findViewById(R.id.editText);
        String newItem = editText.getText().toString();
        if (!newItem.isEmpty() && addNew) {
            // adds item to list after it is added to the database and returned.
            myList.add(ds.createItem(newItem, listID));
            // Updates the listview
            adapter.notifyDataSetChanged();
            // Popup that notifies user that the item was added.
            Toast.makeText(this, String.valueOf(newItem + " added to list."), Toast.LENGTH_SHORT).show();
            editText.setText("");
            // Sets the view to the bottom of the list.
            listview.setSelection(adapter.getCount() - 1);
        }
        if (!newItem.isEmpty() && !addNew) {
            editItem.setItemName(newItem);
            adapter.modifyItem(editItem);
            adapter.notifyDataSetChanged();
            addNew = true;
            Button button = (Button) findViewById(R.id.button);
            button.setText("Add");
            editText.setText("");
            editText.clearFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(),0);
            Toast.makeText(this, String.valueOf(newItem + " has been edited."), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Creates a menu in the top menu bar to select further activities
     *
     * @param menu Meny
     * @return true to kill event
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    /**
     * Handles the click events for the menu buttons
     *
     * @param item selected item
     * @return true to consume the click event here
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete:
                // Make alert dialog for delete confirmation
                AlertDialog.Builder alert= new AlertDialog.Builder(this);
                alert.setTitle("Delete Entry");
                alert.setMessage("Are you sure you want to delete "
                        + ds.getSelectedItemcount(listID)
                        + "?");
                // Confirm delete action
                alert.setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                adapter.deleteChecked();
                                adapter.notifyDataSetChanged();
                            }
                        });
                // Cancel delete action
                alert.setNegativeButton(android.R.string.no,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                alert.show();

                break;

            case R.id.capture:
                Intent intent = new Intent(this, OcrCaptureActivity.class);
                intent.putExtra(OcrCaptureActivity.AutoFocus, autoFocus);
                intent.putExtra(OcrCaptureActivity.UseFlash, useFlash);

                startActivityForResult(intent, RC_OCR_CAPTURE);
        }

        return true;
    }

    /**
     * Create context menu for item
     * @param menu context menu
     * @param v view
     * @param menuInfo contextMenuInfo
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.itemcontext, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        editItem = myList.get(info.position);
        switch (item.getItemId()){
            case R.id.contextedit:
                EditText editText = (EditText) findViewById(R.id.editText);
                editText.setText(editItem.getItemName());
                addNew = false;
                Button button = (Button) findViewById(R.id.button);
                button.setText(getText(R.string.edit_item));
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(listview, InputMethodManager.SHOW_IMPLICIT);
                editText.requestFocus();
                editText.setSelection(editItem.getItemName().length());
                return true;
            case R.id.contextdelete:
                Toast.makeText(this, "delete " + editItem.getItemName(), Toast.LENGTH_LONG).show();
                adapter.deleteItem(editItem);
                adapter.notifyDataSetChanged();
                return true;
            default:
                Toast.makeText(this, "What?", Toast.LENGTH_LONG).show();
                return true;
        }
    }

    // For returning camera capture

    /**
     * Called when an activity you launched exits, giving you the requestCode
     * you started it with, the resultCode it returned, and any additional
     * data from it.  The <var>resultCode</var> will be
     * {@link #RESULT_CANCELED} if the activity explicitly returned that,
     * didn't return any result, or crashed during its operation.
     * <p/>
     * <p>You will receive this call immediately before onResume() when your
     * activity is re-starting.
     * <p/>
     *
     * @param requestCode The integer request code originally supplied to
     *                    startActivityForResult(), allowing you to identify who this
     *                    result came from.
     * @param resultCode  The integer result code returned by the child activity
     *                    through its setResult().
     * @param data        An Intent, which can return result data to the caller
     *                    (various data can be attached to Intent "extras").
     * @see #startActivityForResult
     * @see #createPendingResult
     * @see #setResult(int)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_OCR_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Toast.makeText(this, R.string.ocr_success, Toast.LENGTH_SHORT).show();
                    String text = data.getStringExtra(OcrCaptureActivity.TextBlockObject);
                    String textStr[] = text.split("\\r\\n|\\n|\\r");
                    ds.open();
                    int len = textStr.length;
                    for (int i = 0; i < len; i++) {
                        myList.add(ds.createItem(textStr[i], listID));
                    }
                    adapter.notifyDataSetChanged();
                    Toast.makeText(this, "Multiple items added:\n" + text, Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Text read: " + text);
                } else {
                    Toast.makeText(this, R.string.ocr_failure, Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "No Text captured, intent data is null");
                }
            } else {
                Toast.makeText(this, (String.format(getString(R.string.ocr_error),
                        CommonStatusCodes.getStatusCodeString(resultCode))),
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
