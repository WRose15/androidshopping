package com.rosecorp.shoppinglist;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

public class ListGroupActivity extends ListActivity {

    List<ListModel> listOfLists;
    ListView mainView;
    MainListViewAdapter adapter;
    private GroceryDataSource ds;
    ListModel editListName;

    EditText editList;
    Button addListButton;

    Boolean addNewList;


    public static final String LIST_ID = "com.rosecorp.number";
    public static final String LIST_NAME = "com.rosecorp.shoppinglist";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_group);

        setTitle(getText(R.string.activity_main));
        ds = new GroceryDataSource(this);
        ds.open();
        listOfLists = ds.getAllLists();

        addNewList = true;

        adapter = new MainListViewAdapter(this, listOfLists, ds);
        mainView = getListView();
        mainView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        viewList2(id);
                    }
                });

        setListAdapter(adapter);

        //Hide editText box abd add button until needed
        editList = (EditText) findViewById(R.id.editList);
        addListButton = (Button) findViewById(R.id.addListButton);

        editList.setVisibility(View.GONE);
        addListButton.setVisibility(View.GONE);

        registerForContextMenu(mainView);
    }

    /**
     * Creates a menu to select further options
     *
     * @param menu menu
     * @return true to consume the press event
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.lists, menu);
        return true;
    }

    /**
     * Select a menu item, currently only able to add list.
     * @param item Menu option
     * @return true to consume press event
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addList:
                focusKeyboard();
                return true;
        }
        return true;
    }

    /**
     * Add a new list to the main view or change the name.
     * @param view this
     */
    public void addList(View view)
    {
        String newList = editList.getText().toString();
        if( !newList.isEmpty() && addNewList )
        {
            listOfLists.add(ds.createList(newList));
            adapter.notifyDataSetChanged();
            editList.setText("");
            editList.clearFocus();
            mainView.setSelection(listOfLists.size() - 1);
            clearKeyboard(view);
        }
        if( !newList.isEmpty() && !addNewList )
        {
            editListName.setListName(editList.getText().toString());
            adapter.modifyList(editListName);
            adapter.notifyDataSetChanged();
            Toast.makeText(this, String.valueOf(newList + "'s name changed."),
                    Toast.LENGTH_SHORT).show();
            addNewList = true;
            addListButton.setText(getText(R.string.add_item));
            editList.setText("");
            editList.clearFocus();
            clearKeyboard(view);
        }
    }

    /**
     * Starts the IndividualList activity to view the items associated with the selected list
     * when button is clicked
     * @param v View the list is in.
     */
    public void viewList(View v)
    {
        Intent intent = new Intent(this, IndividualList.class);
        long listID = ((ListModel)v.getTag()).getListID();
        String name = ((ListModel) v.getTag()).getListName();
        intent.putExtra(LIST_ID, listID);
        intent.putExtra(LIST_NAME, name);
        startActivity(intent);
    }

    void viewList2(long id)
    {
        Intent intent = new Intent(this, IndividualList.class);
        long listID = listOfLists.get((int)id).getListID();
        String name = listOfLists.get((int)id).getListName();
        intent.putExtra(LIST_ID, listID);
        intent.putExtra(LIST_NAME, name);
        startActivity(intent);
    }

    /**
     * Hides the keyboard, input box and add button from view
     * @param v view
     */
    public void clearKeyboard(View v)
    {
        InputMethodManager imm =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        editList.setVisibility(View.GONE);
        addListButton.setVisibility(View.GONE);
    }

    /**
     * Set focus to editText box, unhide box and button
     */
    public void focusKeyboard()
    {
        editList.setVisibility(View.VISIBLE);
        addListButton.setVisibility(View.VISIBLE);
        InputMethodManager imm =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mainView, InputMethodManager.SHOW_IMPLICIT);
        editList.requestFocus();
    }

    /**
     * Create context menu for list of lists.
     * @param menu menu
     * @param v view
     * @param menuInfo  menuinfo
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.itemcontext, menu);
    }

    /**
     * Handles the context menu commands.
     * @param item which item it is in the list
     * @return true for event handled.
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        editListName = listOfLists.get(info.position);
        switch (item.getItemId()) {
            case R.id.contextedit:
                editList.setText(editListName.getListName());
                addNewList = false;
                addListButton.setText(getText(R.string.edit_item));
                focusKeyboard();
                editList.setSelection(editListName.getListName().length());
                return true;
            case R.id.contextdelete:
                // Make alert dialog for delete confirmation
                AlertDialog.Builder alert= new AlertDialog.Builder(this);
                alert.setTitle("Delete Entry");
                alert.setMessage("Are you sure you want to delete "
                        + editListName.getListName()
                        + "?");
                // Confirm delete action
                alert.setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                    public void onClick (DialogInterface dialog, int id) {
                        adapter.deleteList(editListName);
                        adapter.notifyDataSetChanged();
                        Toast.makeText(getApplicationContext(), editListName.getListName() + " deleted.",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                // Cancel delete action
                alert.setNegativeButton(android.R.string.no,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                alert.show();
                return true;
            default:
                Toast.makeText(this, "What?", Toast.LENGTH_SHORT).show();
                return true;
        }
    }

    // Android state change behaviors
    @Override
    protected void onResume() {
        super.onResume();
        ds.open();
        adapter.notifyDataSetChanged();
    }

    protected void onPause() {
        super.onPause();
        ds.close();
    }
}
